
package ec.edu.espol.util;
//import espol.edu.ec.common.Comprador;
/*import espol.edu.ec.common.Vehiculo;
import espol.edu.ec.common.Vendedor;
import espol.edu.ec.common.Venta;
*/
import ec.edu.espol.model.Oferta;
import java.util.Collections;
import ec.edu.espol.model.Cliente;
import ec.edu.espol.model.Venta;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Util {
    private Util(){    
    }
    
    public static boolean isDigit(String s){
        try{
            if(Integer.parseInt(s)>=0)
                return true;
            return false;
        }
        catch(Exception e){
            return false;
        }
    }

    public static boolean isLetras(String s){

        String sUp= s.toUpperCase();
        int indicador=0;
        for(int i= 0; i<sUp.length();i++){
            char caracter= sUp.charAt(i);
            if(caracter>=65&&caracter<=90)
                indicador++;
        }
        if(indicador==sUp.length())
            return true;
        else
            return false;
    }
    
    public static void serializarPersonas(ArrayList<Cliente> personas, String nomFile){
        try(FileOutputStream f= new FileOutputStream(nomFile);
                ObjectOutputStream out= new ObjectOutputStream(f)){
            out.writeObject(personas);
        } 
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static ArrayList<Cliente> deserealizarPersonas(String nomFile){
        ArrayList<Cliente> personas= new ArrayList<>();
        try(FileInputStream f= new FileInputStream(nomFile);
                ObjectInputStream in= new ObjectInputStream(f)){
            personas= (ArrayList<Cliente>)in.readObject();
            return personas;
        } 
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return personas;
    }
    
     public static void serializarVentas(ArrayList<Venta> ventas, String nomFile){
        try(FileOutputStream f= new FileOutputStream(nomFile);
                ObjectOutputStream out= new ObjectOutputStream(f)){
            out.writeObject(ventas);
        } 
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
     
    public static ArrayList<Venta> deserealizarVentas(String nomFile){
        ArrayList<Venta> ventas= new ArrayList<>();
        try(FileInputStream f= new FileInputStream(nomFile);
                ObjectInputStream in= new ObjectInputStream(f)){
            ventas= (ArrayList<Venta>)in.readObject();
            System.out.println(ventas);
            return ventas;
        } 
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return ventas;
    }
   
    public static boolean usuarioValido(String usuario, String clave, String nomfile) throws NoSuchAlgorithmException{
        ArrayList<Cliente> personas= Util.deserealizarPersonas(nomfile);
        for(Cliente p: personas){
            if(usuario.equals(p.getCuenta().getUsuario())&&clave.equals(p.getCuenta().getClave()))
                return true;
        }
        return false;
    }
    
    public static boolean validarCorreo(String correo,String nomArch){
        ArrayList<Cliente> datos= Util.deserealizarPersonas(nomArch);
        for(Cliente p: datos){
            String correoC= p.getCuenta().getCorreoElectronico();
            if(correoC.equals(correo)){
                return true;
            }
        }
        return false ;    
    }
    public static boolean  validarUsuario(String usuario,String nomArch){
         ArrayList<Cliente> datos= Util.deserealizarPersonas(nomArch);
        for(Cliente p: datos){
            String usuarioC= p.getCuenta().getUsuario();
            if(usuarioC.equals(usuario)){
                return true;
            }
        }
        return false ;
    }
    
    public static Cliente obtenerPersona(String usuario, String nomFile) throws Exception {
        
        ArrayList<Cliente> personas= Util.deserealizarPersonas(nomFile);
        for(Cliente p: personas){
            if(usuario.equals(p.getCuenta().getUsuario()))
                return p;
        }
        
        throw new Exception("Usuario no encontrado");
    }
   
    public static boolean placaExistente(String placa,String nomArch){
        ArrayList<Venta> listaVentas= Util.deserealizarVentas(nomArch);
        System.out.println("placaentra");
        ArrayList<String> placas= new ArrayList<>();
        for(Venta ventaC: listaVentas){
            String placaC= ventaC.getVehiculo().getPlaca();
            placas.add(placaC);
            System.out.println(placas);
            for(String p:placas){
                if(p.equals(placa))
                    return true;   
        }    
       
        }return false;
    }
    public static ArrayList<String> leerProperties(String nomFile){
        ArrayList<String> parametros= new ArrayList<>();
        File archivo = new File(nomFile);
        try (InputStream inputStream = new FileInputStream(archivo)) {
                Properties prop = new Properties();
                prop.load(inputStream);
                String correo= prop.getProperty("correo");
                String clave= prop.getProperty("clave");
                String puerto= prop.getProperty("puerto");
                String servidor= prop.getProperty("servidor");
                parametros.add(correo);
                parametros.add(clave);
                parametros.add(puerto);
                parametros.add(servidor);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return parametros;
    }
    
    public static ArrayList<Oferta> ofertasOrdenadas(ArrayList<Oferta> ofertas){
        ArrayList<Oferta> ofertasO= new ArrayList<>();
        ArrayList<Integer> maximo= new ArrayList<>();
        for(Oferta of: ofertas)
            maximo.add((int)of.getValor_ofertado());
        while(ofertasO.size()!=ofertas.size()){
            for(Oferta of: ofertas){
                if(!maximo.isEmpty()){
                    Integer precio = Collections.max(maximo);
                    if(precio==of.getValor_ofertado()){
                        ofertasO.add(of);
                        maximo.remove(precio);}
                }
            }
        }
        return ofertasO;
    }

    public static void enviarCorreo(Oferta oferta, String nomfile){
        ArrayList<String> parametros= Util.leerProperties(nomfile);
        String notificación= "Se ha aceptado su oferta";
        String cuerpo= "Su oferta de $"+oferta.getValor_ofertado()+" ha sido aceptada para: "+oferta.getVenta().getVehiculo().getMarca()+
                       "-"+oferta.getVenta().getVehiculo().getModelo()+", Placa: "+oferta.getVenta().getVehiculo().getPlaca();
        String remitente= parametros.get(0); 
        Properties props = System.getProperties();
        props.put("mail.smtp.host",parametros.get(3)); 
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.clave",parametros.get(1));
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", parametros.get(2));  
        Session session = Session.getInstance(props);
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(remitente));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress((oferta.getComprador().getCuenta().getCorreoElectronico())));   //Se podrían añadir varios de la misma manera
            message.setSubject(notificación);
            message.setText(cuerpo);
            Transport transport = session.getTransport("smtp");
            transport.connect(parametros.get(3), remitente, parametros.get(1));
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me) {
            me.printStackTrace();   //Si se produce un error
        }
    }
    public static boolean placaValida(String placa){
        if(placa.length()==7&&Util.isLetras(placa.substring(0,3))&&Util.isDigit(placa.substring(3))){
            System.out.println("pasa");
            return true;
        }
        else if(placa.length()==6&&Util.isLetras(placa.substring(0,3))&&Util.isDigit(placa.substring(3))){
            System.out.println("pasa2");
            return true;
        }
        return false;
    }
    
    public static boolean tipoVehiculoValido(String tipo){
        String tipoU= tipo.toUpperCase();
        if(tipoU.equals("CARRO")||tipoU.equals("CAMION")||tipoU.equals("CAMIONETA")||tipoU.equals("MOTOCICLETA")){
            return true;
        }
        return false;
    }
    
    public static boolean combustibleValido(String combustible){
        String combustibleU= combustible.toUpperCase();
        if(combustibleU.equals("GASOLINA")||combustibleU.equals("DIESEL")||combustibleU.equals("ELECTRICIDAD")){
            return true;
        }
        return false;
    }
    
    public static boolean vidrioValido(String vidrio){
        String vidrioU= vidrio.toUpperCase();
        if(vidrioU.equals("MANUAL")||vidrioU.equals("ELECTRICO"))
            return true;
        return false;
    }
    
    
    public static boolean traccionValida(String traccion){
        String traccionU= traccion.toUpperCase();
        if(traccionU.equals("DELANTERA")||traccionU.equals("TRASERA")||traccionU.equals("INTEGRAL"))
            return true;
        return false;
    }
    
    public static boolean validarIngreso(String user, String password, ArrayList<Cliente> compradores) throws NoSuchAlgorithmException{
        String cn="";
            for(Cliente e: compradores){
                if(e.getCuenta().getUsuario().equals(user)){
                    cn= e.getCuenta().getClave();
                }
                                 
            }
            for(Cliente e: compradores){
                System.out.println(e);
            }
        String cv=Hash.EncriptarPassword(password);
        System.out.println(cv);
        System.out.println(cn);
        return cn.equals(cv);}
   
}
