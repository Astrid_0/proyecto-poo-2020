/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import ec.edu.espol.util.Util;
import ec.edu.espol.util.Hash;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Hogar
 */
public class Cuenta implements Serializable{
    private String correoElectronico;
    private String organizacion;
    private String usuario;
    private String clave;
    private String tipoCuenta;
    private static final long serialVersionUID= 8912345451999728838L;

    public Cuenta(String correoElectronico, String organizacion, String usuario, String clave, String tipoCuenta) {
        this.correoElectronico = correoElectronico;
        this.organizacion = organizacion;
        this.usuario = usuario;
        this.clave = clave;
        this.tipoCuenta = tipoCuenta;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }


    
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
       if(!Util.validarCorreo(correoElectronico,"comprador.txt"))
            this.correoElectronico = correoElectronico;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        if(!Util.validarUsuario(usuario,"comprador.txt"))
            this.usuario = usuario;
    }

    public String getClave() throws NoSuchAlgorithmException {
        return Hash.EncriptarPassword(clave);
    }
    
    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public String toString() {
        return  " usuario: " + usuario+ ",correoElectronico: " + correoElectronico + ", organizacion: " + organizacion;
    }
    
    
    
}
