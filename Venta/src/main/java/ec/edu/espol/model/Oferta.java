
package ec.edu.espol.model;

import java.io.Serializable;

        
public class Oferta implements Serializable{
    private double valor_ofertado;
    private Cliente comprador;
    private Venta venta;
    private static final long serialVersionUID= 8909856450123451838L;

    public Oferta(double valor_ofertado, Cliente comprador, Venta venta){
        this.valor_ofertado= valor_ofertado;
        this.comprador= comprador;
        this.venta= venta;
    }

    public double getValor_ofertado() {
        return valor_ofertado;
    }

    public Cliente getComprador() {
        return comprador;
    }

    public Venta getVenta() {
        return venta;
    }
    
    @Override
    public String toString(){
        return "Comprador: "+this.comprador.getCuenta().getCorreoElectronico()+" ,Valor ofertado: $"+this.valor_ofertado;
    }
}
