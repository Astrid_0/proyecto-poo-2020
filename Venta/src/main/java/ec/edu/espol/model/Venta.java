/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Hogar
 */
public class Venta implements Serializable{
    private Vehiculo vehiculo;
    private Cliente vendedor;
    public ArrayList<Oferta> misOfertas= new ArrayList<>();
    private static final long serialVersionUID= 1234856450987721838L;
    
    public Venta(Vehiculo vehiculo, Cliente vendedor, ArrayList<Oferta> misOfertas){
        this.vehiculo= vehiculo;
        this.vendedor= vendedor;
        this.misOfertas= misOfertas;
    }
    
    public Vehiculo getVehiculo() {
        return vehiculo;
    }
    public Cliente getVendedor() {
        return vendedor;
    }

    public ArrayList<Oferta> getMisOfertas() {
        return misOfertas;
    }

    public void setMisOfertas(ArrayList<Oferta> misOfertas) {
        this.misOfertas = misOfertas;
    }
    
  
    @Override
    public String toString(){
        return ""+this.vehiculo+", Vendedor: "+this.vendedor.getNombre()+" - "+this.vendedor.getCuenta().getCorreoElectronico();
    }
}
