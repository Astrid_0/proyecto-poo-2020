                      
package ec.edu.espol.model;

import ec.edu.espol.util.Util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
public abstract class Vehiculo implements Serializable{
    protected String placa;
    protected String marca;
    protected String modelo;
    protected String tipo_de_motor;
    protected String año;
    protected String color;
    protected String tipo_de_combustible;
    protected String transmisión;
    protected double recorrido;
    protected double precio;
    protected String tv;
    protected String img;
    public static ArrayList<Venta> vehiculosEnVenta= new ArrayList<>();
    private static final long serialVersionUID= 8999612343629388838L;
    
    public Vehiculo(String placa, String marca, String modelo, String tipo_de_motor, String año, String color,
                    String tipo_de_combustible, String transmisión, double recorrido, double precio, String img, String tv){
        this.placa= placa;
        this.año= año;
        this.color= color;
        this.modelo= modelo;
        this.marca= marca;
        this.precio= precio;
        this.recorrido= recorrido;
        this.transmisión= transmisión;
        this.tipo_de_combustible= tipo_de_combustible;
        this.tipo_de_motor= tipo_de_motor;
        this.img=img;
        this.tv=tv;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        if(Util.placaValida(placa))
            this.placa= placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTipo_de_motor() {
        return tipo_de_motor;
    }

    public void setTipo_de_motor(String tipo_de_motor) {
        this.tipo_de_motor = tipo_de_motor;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        if(Util.isDigit(año))
            this.año = año;
    }
    
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipo_de_combustible() {
        return tipo_de_combustible;
    }

    public void setTipo_de_combustible(String tipo_de_combustible) {
        if(Util.combustibleValido(tipo_de_combustible))
            this.tipo_de_combustible = tipo_de_combustible;
    }

    public String getTransmisión() {
        return transmisión;
    }

    public double getRecorrido() {
        return recorrido;
    }

    public double getPrecio() {
        return precio;
    }

    public void setRecorrido(double recorrido) {
        this.recorrido = recorrido;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    public String getTipoVehiculo() {
        return tv;
    }

    public String getImg() {
        return img;
    }
    

    public void setTransmisión(String transmisión) {
        this.transmisión = transmisión;
    }
    public static ArrayList  filtrarMotos(ArrayList<Venta> ventasG){
        ArrayList<Venta> motos= new ArrayList<>();
        for( Venta v: ventasG){
            if (v.getVehiculo() instanceof Motocicleta){
                motos.add(v);
        }
        }
        return motos;
    }
    
    public static ArrayList  filtrarCarros(ArrayList<Venta> ventasG){
        ArrayList<Venta> carros= new ArrayList<>();
     
        for( Venta v: ventasG){
            if(v.getVehiculo() instanceof Carro){
            carros.add(v);
        }}
        return carros;
    
    }
    public static ArrayList  filtrarCamion(ArrayList<Venta> ventasG){
        ArrayList<Venta> camiones= new ArrayList<>();
     
        for( Venta v: ventasG){
            if(v.getVehiculo() instanceof Camion){
            camiones.add(v);
        }}
        return camiones;
    
    }
   public static ArrayList  filtrarCamionetas(ArrayList<Venta> ventasG){
        ArrayList<Venta> camionetas= new ArrayList<>();
     
        for( Venta v: ventasG){
            if(v.getVehiculo() instanceof Camioneta){
            camionetas.add(v);
        }}
        return camionetas;
    
    }
  
    
    public static ArrayList filtrarRecorrido(double inicio, double fin,ArrayList<Venta> vehiculosEnVenta){
        
        ArrayList<Venta> vehiculosR= new ArrayList<>();
        try{
        for(Venta v: vehiculosEnVenta){
            if(v.getVehiculo().recorrido>=inicio && v.getVehiculo().recorrido<=fin){
               vehiculosR.add(v);
            }
        }}
         catch(NumberFormatException e){
            System.out.println("Parametros incorrectos");
        }
        
        return vehiculosR;
    }
    
     public static ArrayList filtrarAnio(int inicio, int fin,ArrayList<Venta> vehiculosEnVenta){
        ArrayList<Venta> vehiculosR= new ArrayList<>();
        try{     
        for(Venta v: vehiculosEnVenta){
            if(Integer.valueOf(v.getVehiculo().año)>=inicio && Integer.valueOf(v.getVehiculo().año)<=fin){
               vehiculosR.add(v);
            }
        }}
         catch(NumberFormatException e){
            System.out.println("Parametros incorrectos");
        }
        return vehiculosR;}
    
    public static ArrayList filtrarPrecio(double inicio, double fin,ArrayList<Venta> vehiculosEnVenta){
        
        ArrayList<Venta> vehiculosR= new ArrayList<>();
        try{
        for(Venta v: vehiculosEnVenta){
            if(v.getVehiculo().precio>=inicio && v.getVehiculo().precio<=fin){
               vehiculosR.add(v);
            }
        }}
         catch(NumberFormatException e){
            System.out.println("Parametros incorrectos");
        }
        return vehiculosR;
    }
    
    
    
    @Override
    public String toString(){
        return "Vehículo: Placa: "+this.placa+", Marca: "+this.marca+", Modelo: "+this.modelo+", Tipo de motor: "+this.tipo_de_motor+", Año: "+this.año+", Color: "+this.color+
                ", Tipo de combustible: "+this.tipo_de_combustible+", Transmisión: "+this.transmisión+", Recorrido: "+this.recorrido+", Precio: "+this.precio;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;
        if (!Objects.equals(this.placa, other.placa)) {
            return false;
        }
        return true;
    }
    
    
    
   
}
