
package ec.edu.espol.model;

import ec.edu.espol.util.Util;
import java.util.ArrayList;


public class MainPrueba {
    public static void main(String[] args){
        Cliente c= new Cliente("Giovanny","Romero", new Cuenta("correo","organización","gio","12345","Ambos"));
        Cliente c2= new Cliente("Paula","Romero", new Cuenta("romerovizuetagiovannyandres@gmail.com","organización","pau","54321","Ambos"));
        Vehiculo v= new Carro("tba3289","nissan","sunny","otto","2020","rojo","manual","gasolina","integral",23456,12345,4,"img","Carro");
        Vehiculo v2= new Carro("grx3428","nissan","sentra","otto","2020","rojo","manual","gasolina","integral",23456,20000,4,"img","Carro");
        Vehiculo v3= new Carro("jth3289","nissan","sunny","otto","2020","rojo","manual","gasolina","integral",23456,12345,4,"img","Carro");
        Vehiculo v4= new Carro("rst3289","nissan","sunny","otto","2020","rojo","manual","gasolina","integral",23456,12345,4,"img","Carro");
        ArrayList<Cliente> clientes= Util.deserealizarPersonas("personas.ser");
        clientes.add(c);
        clientes.add(c2);
        Util.serializarPersonas(clientes, "personas.ser");
        Venta ve= new Venta(v,c,new ArrayList<Oferta>());
        Venta ve2= new Venta(v2,c2,new ArrayList<Oferta>());
        Venta ve3= new Venta(v3,c,new ArrayList<Oferta>());
        Venta ve4= new Venta(v4,c,new ArrayList<Oferta>());
        ArrayList<Venta> ventas2= new ArrayList<>();
        ventas2.add(ve2);
        c2.ofertar(10000, ve);
        c2.ofertar(250000, ve);
        c2.ofertar(12000, ve4);
        c2.ofertar(12345, ve); 
        c2.ofertar(30000, ve3);
        ventas2.add(ve);
        ventas2.add(ve4);
        ventas2.add(ve3);
        Util.serializarVentas(ventas2, "ventas.ser");      
    }
}
