/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import ec.edu.espol.util.Util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author DELL i5
 */
public class Cliente implements Serializable {
    private String nombre;
    private String apellido;
    private Cuenta cuenta;
   
    private static final long serialVersionUID= 8909856450987721838L;
    
    public Cliente(String nombre, String apellido, Cuenta cuenta){
        this.nombre= nombre;
        this.apellido= apellido;
        this.cuenta= cuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public Cuenta getCuenta() {
        return cuenta;
    }
    
    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
    public void ofertar(double valorOfertado, Venta v){
        ArrayList<Venta> ventas= Util.deserealizarVentas("ventas.ser");
        ArrayList<Oferta> ofertas=new ArrayList<>();
        int ind=0;                        
        for (int i = 0; i < ventas.size(); i++) {
            if(ventas.get(i).getVehiculo().getPlaca().equals(v.getVehiculo().placa)){
                ofertas= v.getMisOfertas();
                Oferta oferta= new Oferta(valorOfertado,this,v);
                ofertas.add(oferta);
                ind=i;
            }
                }       
        v.setMisOfertas(ofertas);
        ventas.get(ind).setMisOfertas(ofertas);
        Util.serializarVentas(ventas, "ventas.ser");
    }
    
    public ArrayList<Venta> obtenerMisVentas(){
        ArrayList<Venta> ventas= Util.deserealizarVentas("ventas.ser");
        ArrayList<Venta> misVentas= new ArrayList<>();
        for(Venta ven: ventas){
            if(ven.getVendedor().getCuenta().getUsuario().equals(this.getCuenta().getUsuario())){
                misVentas.add(ven);
            }
        }
        return misVentas;
    }
    
    public ArrayList<Oferta> obtenerOfertas(Venta v){
        ArrayList<Venta> ventas= this.obtenerMisVentas();
        ArrayList<Oferta> ofertas= new ArrayList<>();
        for(Venta mv: ventas){
            if(mv.getVehiculo().getPlaca().equals(v.getVehiculo().getPlaca())) 
                ofertas= mv.getMisOfertas();
        }
        return ofertas;
    }
    
    @Override
    public String toString(){
        return "Persona: Nombre: "+this.nombre+", Apellido: "+this.apellido + ", Cuenta: "+ this.cuenta;
    }
    @Override
    public int hashCode(){
        return Objects.hash(this.cuenta.getCorreoElectronico());
    }
     @Override
    public boolean equals(Object o){
    if(o==null || o.getClass()!=this.getClass())
        return false;
    if(o==this)
        return true;
    Cliente p= (Cliente)o;
    return p.cuenta.getCorreoElectronico().equals(this.cuenta.getCorreoElectronico()) && p.cuenta.getUsuario().equals(this.cuenta.getUsuario());
    
    }
}
