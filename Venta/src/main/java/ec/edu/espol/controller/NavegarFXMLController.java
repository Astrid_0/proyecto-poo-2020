/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import static ec.edu.espol.gui.App.loadFXML;
import ec.edu.espol.model.Cliente;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.model.Venta;
import ec.edu.espol.util.Util;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Hogar
 */
public class NavegarFXMLController implements Initializable {

    @FXML
    private ComboBox comboFiltro;
    @FXML
    private Button botv;

    /**
     * Initializes the controller class.
     */
    static String identificacion;
    @FXML
    private FlowPane flowpane;
    
    @FXML
    private HBox pane2;
    @FXML
    private AnchorPane scrollImagenes;
    @FXML
    private AnchorPane scrollInfo;
    @FXML
    private AnchorPane anchorG;
    ArrayList<Venta> ventas;
    VBox vbx= new VBox();
    Cliente c;
    
    

    public AnchorPane getAnchorG() {
        return anchorG;
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            c= Util.obtenerPersona(identificacion, "personas.ser");
            ventas= quitarMisVentas(c);
            presentarVentas(ventas);
            llenarCombo();
        } catch (Exception ex) {
            ex.printStackTrace();}      }    
    private ArrayList<Venta> quitarMisVentas(Cliente c){
        ArrayList<Venta> ventasNf= Util.deserealizarVentas("ventas.ser");   
        ArrayList<Venta> navegarVentas= new ArrayList<>();
        for(Venta v: ventasNf){               
                if(!(c.getCuenta().getUsuario().equals(v.getVendedor().getCuenta().getUsuario()))){
                    navegarVentas.add(v);
            }}
        return navegarVentas;
    }
    static Venta venta;
    static Cliente clie;
    static double ofertaV;
    private void presentarVentas(ArrayList<Venta>navegarVenta){
        String url;
        url="C:\\Users\\ronal\\Desktop\\bsc.jpg";
            for(Venta va: navegarVenta){
                Button bofer= new Button("Ofertar");
                url= va.getVehiculo().getImg();    
                HiloImagen h= new HiloImagen(url);
                h.start();
                Text txt= new Text(va.toString());
                
                vbx.getChildren().addAll(txt,bofer);
                scrollInfo.setMaxSize(anchorG.USE_PREF_SIZE, anchorG.USE_PREF_SIZE);
                bofer.setOnMouseClicked(new EventHandler<MouseEvent>(){
                    
                    @Override public void handle(MouseEvent arg0){
                        
                        
                        Stage s= new Stage();
                        try {
                            OfertaPrecioFXMLController.cli=c;
                            OfertaPrecioFXMLController.v=va;
                            Scene of= new Scene(loadFXML("OfertaPrecioFXML"));
                            s.setScene(of);                                                  
                            s.show();
                            
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        
                    }});
                scrollInfo.getChildren().add(vbx);
            }
    
    
    }
    private ArrayList<String> tiposVehiculos(ArrayList<Venta>navVentas){
        ArrayList<String> tiposVehiculos= new ArrayList<>();
        for(Venta va: navVentas){ 
            if(!tiposVehiculos.contains(va.getVehiculo().getTipoVehiculo())){
                tiposVehiculos.add(va.getVehiculo().getTipoVehiculo());
                }
        }
 
       return tiposVehiculos;
    }
    private void llenarCombo(){
        ArrayList<String> opcionesFiltro= new ArrayList<>();
        opcionesFiltro.add("Año");
        opcionesFiltro.add("Precio");
        opcionesFiltro.add("Recorrido");
        opcionesFiltro.add("Tipo de Vehiculo");
        comboFiltro.setItems(FXCollections.observableArrayList(opcionesFiltro));
    }
    @FXML
    private void mostrarFiltros(ActionEvent event) {
        vbx.getChildren().clear();                   
        pane2.getChildren().clear();
        TextField desde= new TextField(); TextField hasta= new TextField(); Button buscar= new Button("Filtrar");
        ComboBox combi= (ComboBox)event.getSource(); String tipoFiltro= (String)combi.getValue();
        if(tipoFiltro.equals("Año")){
            pane2.getChildren().addAll(new Text("Desde:"),desde ,new Text("Hasta:"),hasta ,buscar);
            buscarAnio(buscar,desde,hasta);
        }
        else if(tipoFiltro.equals("Precio")){
            pane2.getChildren().addAll(new Text("Desde:"),desde ,new Text("Hasta:"),hasta ,buscar);
            buscarPrecio(buscar,desde,hasta);
        }
        else if(tipoFiltro.equals("Recorrido")){
            pane2.getChildren().addAll(new Text("Desde:"),desde ,new Text("Hasta:"),hasta ,buscar);
            buscarRecorrido(buscar,desde,hasta);
        }
        else{   
            ComboBox combiTipo= new ComboBox();
            combiTipo.setItems(FXCollections.observableArrayList(tiposVehiculos(ventas))); combiTipo.getSelectionModel().selectFirst(); 
            pane2.getChildren().addAll(combiTipo,buscar);  
            buscar.setOnMouseClicked(new EventHandler<MouseEvent>(){
                                        @Override public void handle(MouseEvent arg0){
                                        String valor=(String)combiTipo.getValue();
                                        mostrarFiltroCom(valor) ; 
                                        }});
        }
    }
    private void mostrarFiltroCom(String tipo){
        switch(tipo){
                case "Carro":
                    ArrayList<Venta> ventaC= Vehiculo.filtrarCarros(ventas);
                    if(ventaC.size()==0){mostrarAlerta("No se encontraron Datos");}
                    presentarVentas(ventaC);
                    break;
                case "Camion":
                    ArrayList<Venta> ventaCa= Vehiculo.filtrarCamion(ventas);
                    if(ventaCa.size()==0){ mostrarAlerta("No se encontraron Datos");}
                    presentarVentas(ventaCa);
                    break;
                case "Camioneta":
                    ArrayList<Venta> ventaCam= Vehiculo.filtrarCamionetas(ventas);
                    if(ventaCam.size()==0){ mostrarAlerta("No se encontraron Datos"); }
                    presentarVentas(ventaCam);
                    break;
                case "Motocicleta":
                    ArrayList<Venta> ventaM= Vehiculo.filtrarMotos(ventas);
                    if(ventaM.size()==0){ mostrarAlerta("No se encontraron Datos");}
                    presentarVentas(ventaM);
                    break;  
            }   
    }
    private void buscarAnio(Button buscar,TextField desde, TextField hasta){
        buscar.setOnMouseClicked(new EventHandler<MouseEvent>(){
        @Override public void handle(MouseEvent arg0){
        
            if(desde.getText().isEmpty()||hasta.getText().isEmpty()){
            mostrarAlerta("Ingrese Datos");
            }
            else{
            try{
            ArrayList<Venta> ventasA= Vehiculo.filtrarAnio(Integer.valueOf(desde.getText()), Integer.valueOf(hasta.getText()), ventas);
            if(ventasA.size()==0){
                mostrarAlerta("No se encontraron Datos");
            }
            presentarVentas(ventasA);
            }
        catch(NumberFormatException e){
            mostrarAlerta("Ingrese datos numéricos");
        }}
         }});
         
    }
     private void buscarPrecio(Button buscar,TextField desde, TextField hasta){
        
        buscar.setOnMouseClicked(new EventHandler<MouseEvent>(){
             @Override public void handle(MouseEvent arg0){
                
                    if(desde.getText().isEmpty()||hasta.getText().isEmpty()){
                        mostrarAlerta("Ingrese Datos");
                    }
                    else{
                    try{
                    ArrayList<Venta> ventasP=Vehiculo.filtrarPrecio(Double.valueOf(desde.getText()), Double.valueOf(hasta.getText()), ventas);
                    if(ventasP.size()==0){
                        mostrarAlerta("No se encontraron Datos");
                    }
                    presentarVentas(ventasP);}
        catch(NumberFormatException e){
            mostrarAlerta("Ingrese datos numéricos");
        }}
                                        }});
    }
    private void buscarRecorrido(Button buscar,TextField desde, TextField hasta){
        buscar.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override public void handle(MouseEvent arg0){
            
                if(desde.getText().isEmpty()||hasta.getText().isEmpty()){
                mostrarAlerta("Ingrese Datos");
                }
                else{
                try{
                ArrayList<Venta> ventasR=Vehiculo.filtrarRecorrido(Double.valueOf(desde.getText()), Double.valueOf(hasta.getText()), ventas);
                if(ventasR.size()==0){
                    mostrarAlerta("No se encontraron Datos");
                }
                presentarVentas(ventasR);}
                catch(NumberFormatException e){
                    mostrarAlerta("Ingrese datos numéricos");
           
            }}
                                        }});
        
    }
    @FXML
    private void volver(ActionEvent event) {
         
        try {
            InicioFXMLController.ID=identificacion;     
            FXMLLoader fxmlLoader = App.loadFXMLLoad("InicioFXML");
            App.setRoot(fxmlLoader);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
                       
    }
    private void mostrarAlerta(String mensaje){
    Alert alerta= new Alert(Alert.AlertType.INFORMATION,mensaje);
            alerta.show();}

   
    private class HiloImagen extends Thread{
        private String url;

        public HiloImagen(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
         @Override
        public void run() {
            try {
                Image im = new Image(new FileInputStream(url));
                ImageView img= new ImageView(im);
                scrollImagenes.getChildren().add(img);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            }
      
    }
        
    
    }
    
}
