/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.*;
import ec.edu.espol.util.Util;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
/**
 * FXML Controller class
 *
 * @author ronal
 */
public class VehiculoFXMLController implements Initializable {


    @FXML
    private TextField placa;
    @FXML
    private TextField marca;
    @FXML
    private TextField modelo;
    @FXML
    private TextField recorrido;
    @FXML
    private TextField year;
    @FXML
    private TextField color;
    @FXML
    private TextField tp;
    @FXML
    private ComboBox tipovehi;
    @FXML
    private ComboBox trans;
    @FXML
    private ComboBox Tc;
    @FXML
    private TextField precio;
    @FXML
    private TextField url;
    @FXML
    private Text Dp1;
    @FXML
    private Text Dp2;
    @FXML
    private TextField Textdp1;
    @FXML
    private TextField textdp2;
    
    static String Vendedor;
    
    /**
     * Initializes the controller class.
     */
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> opciones= new ArrayList<>();
        opciones.add("Carro");
        opciones.add("Motocicleta");
        opciones.add("Camioneta");
        opciones.add("Camion");
        tipovehi.setItems(FXCollections.observableArrayList(opciones));
        ArrayList<String> tcom= new ArrayList<>();
        tcom.add("Gasolina");
        tcom.add("Electrico");
        tcom.add("Diesel");
        Tc.setItems(FXCollections.observableArrayList(tcom));
        ArrayList<String> transm= new ArrayList<>();
        transm.add("Manual");
        transm.add("Automatico");
        trans.setItems(FXCollections.observableArrayList(transm));
    }    
    
    @FXML
    private void Volver(ActionEvent event) {
        try {
            InicioFXMLController.ID=Vendedor;     
            FXMLLoader fxmlLoader = App.loadFXMLLoad("InicioFXML");
            App.setRoot(fxmlLoader);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }

    private boolean validacionEntrada(){
    if(placa.getText().isEmpty()||modelo.getText().isEmpty()||marca.getText().isEmpty()||recorrido.getText().isEmpty()
           ||year.getText().isEmpty()||tp.getText().isEmpty()||color.getText().isEmpty()||precio.getText().isEmpty()||url.getText().isEmpty()/*||trans.equals("Seleccionar")||Tc.equals("Seleccionar")*/){
            return true;  
        }
    return false;
    }
    private String mens;
    private boolean validacionesDatos(){
        
        if(Util.placaExistente(placa.getText(),"ventas.ser")){
            mens="La Placa que está ingresando ya existe";
            return false;
            }
        else if(!Util.placaValida(placa.getText())){
            mens="La Placa que está ingresando no esta bien escrita";  
            return false;
        }
        else if(!Util.isDigit((recorrido.getText()))||!Util.isDigit((year.getText()))||!Util.isDigit((precio.getText()))){
            mens="Recorrido, Año y precio deben ser datos numericos";
            return false;
        }
        return true;
    }
    private void crearVentaCarro(Cliente c, String t) throws IOException{
        Vehiculo v= new Carro( placa.getText(),  marca.getText(),  modelo.getText(), tp.getText(), year.getText(), 
                   color.getText(), Textdp1.getText(), Tc.getSelectionModel().getSelectedItem().toString(), trans.getSelectionModel().getSelectedItem().toString(),
                        Integer.parseInt(recorrido.getText()), Integer.parseInt(precio.getText()), Integer.parseInt(textdp2.getText()), url.getText(),t);
        ArrayList<Oferta> of= new ArrayList<>();
        ArrayList<Venta> venta= Util.deserealizarVentas("ventas.ser");    
        Venta vventa= new Venta(v,c,of);
        venta.add(vventa);
        Util.serializarVentas(venta, "ventas.ser"); 
        Alert alerta4= new Alert(Alert.AlertType.INFORMATION,"Registro exitoso");
        alerta4.show();
        FXMLLoader fxmlLoader2= App.loadFXMLLoad("InicioFXML");
        App.setRoot(fxmlLoader2);
    }
    private void crearVentCamioneta(Cliente c, String t) throws IOException{
        Vehiculo v1= new Camioneta( placa.getText(),  marca.getText(),  modelo.getText(), tp.getText(), year.getText(), 
                   color.getText(), Textdp1.getText(), Tc.getSelectionModel().getSelectedItem().toString(), trans.getSelectionModel().getSelectedItem().toString(),
                        Integer.parseInt(recorrido.getText()), Integer.parseInt(precio.getText()), textdp2.getText(), url.getText(),t);
        ArrayList<Oferta> of1= new ArrayList<>();
        ArrayList<Venta> venta1= Util.deserealizarVentas("ventas.ser");
        Venta vventa1= new Venta(v1,c,of1);
        venta1.add(vventa1);
        Util.serializarVentas(venta1, "ventas.ser");
        Alert alerta5= new Alert(Alert.AlertType.INFORMATION,"Registro exitoso");
        alerta5.show();
        FXMLLoader fxmlLoader= App.loadFXMLLoad("InicioFXML");
        App.setRoot(fxmlLoader);    
    }
    private void crearVentaMotocicleta(Cliente c, String t) throws IOException{
        Motocicleta moto= new Motocicleta(placa.getText(),marca.getText(),modelo.getText(),tp.getText(), year.getText(), color.getText(),Tc.getSelectionModel().getSelectedItem().toString(),
            trans.getSelectionModel().getSelectedItem().toString(),Integer.parseInt(recorrido.getText()),Integer.parseInt(precio.getText()),Textdp1.getText(),textdp2.getText(),"motocicleta",url.getText());
        Venta ve1= new Venta(moto,c,new ArrayList<Oferta>());
        ArrayList<Venta> ventas= Util.deserealizarVentas("ventas.ser");
        ventas.add(ve1);
        Util.serializarVentas(ventas,"ventas.ser");
        Alert alerta6= new Alert(Alert.AlertType.INFORMATION,"Registro exitoso");
        alerta6.show();
        FXMLLoader fxmlLoader1= App.loadFXMLLoad("InicioFXML");
        App.setRoot(fxmlLoader1);
    
    }
    private void crearVentaCamion(Cliente c, String t) throws IOException{
        Camion camion= new Camion(placa.getText(),marca.getText(),modelo.getText(),tp.getText(), year.getText(), color.getText(),Textdp1.getText(),Tc.getSelectionModel().getSelectedItem().toString(),
                trans.getSelectionModel().getSelectedItem().toString(),Integer.parseInt(recorrido.getText()),Integer.parseInt(precio.getText()),Integer.parseInt(textdp2.getText()),url.getText(),"camion");
        Venta ve2= new Venta(camion,c,new ArrayList<Oferta>());
        ArrayList<Venta> ventas1= Util.deserealizarVentas("ventas.ser");
        ventas1.add(ve2);
        Util.serializarVentas(ventas1,"ventas.ser");
        Alert alerta7= new Alert(Alert.AlertType.INFORMATION,"Registro exitoso");
        alerta7.show();    
        FXMLLoader fxmlLoader4= App.loadFXMLLoad("InicioFXML");
        App.setRoot(fxmlLoader4);
    }
    
    private void mostrarAlerta(String mensaje){
    Alert alerta= new Alert(Alert.AlertType.INFORMATION,mensaje);
            alerta.show();}
    @FXML
    private void Registrar(ActionEvent event) {
        if(validacionEntrada()){ mostrarAlerta("No ha ingresado la informacion");}
        else if(!validacionesDatos()){
        mostrarAlerta(mens);
        }           
        else{
        try{
            String t= (String)tipovehi.getSelectionModel().getSelectedItem();
            Cliente c=Util.obtenerPersona(Vendedor, "personas.ser");
            switch(t){
            case "Carro":
                crearVentaCarro(c,t);
                break;
            case "Camioneta":
                crearVentCamioneta(c,t);  
                break;
            case "Motocicleta":
                crearVentaMotocicleta(c,t);
               break;
            case "Camion":
                crearVentaCamion(c,t);
                break; 
            }
        }catch(Exception e){
            Alert a= new Alert(Alert.AlertType.ERROR, "No se ha grabado su registro"); a.show();
        }}

        }

        @FXML
    private void ActualizarCasillas(ActionEvent event) {
        ComboBox combi= (ComboBox)event.getSource();
        String tipov= (String)combi.getValue();
        switch(tipov){
            case "Carro":insertarAtribCarro(); 
                break;
            case "Camion": insertarAtribCamion();            
                break;
            case "Camioneta":insertarAtribCamioneta();               
                break;
            case "Motocicleta": insertarAtribMotocicleta();             
                break;    
    }}
    private void insertarAtribCarro(){
        Dp1.setText("Vidrios");
        Dp2.setText("Asientos de niños");
    }
    private void insertarAtribCamion(){
        Dp1.setText("Vidrios");
        Dp2.setText("Capacidad de carga");
    }
    private void insertarAtribCamioneta(){
        Dp1.setText("Vidrios");
        Dp2.setText("Traccion");
    }
    private void insertarAtribMotocicleta(){
        Dp1.setText("Tipo de moto");
        Dp2.setText("Sistema de refrigeracion");
    }
    
    @FXML
    private void InsertarImagen(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Buscar Imagen");

        // Agregar filtros para facilitar la busqueda
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );

        // Obtener la imagen seleccionada
        File imgFile = fileChooser.showOpenDialog(null);
        System.out.println(imgFile);
        url.setText(imgFile.getPath());
    }


}
