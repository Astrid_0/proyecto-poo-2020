/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cliente;
import ec.edu.espol.util.Util;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author Hogar
 */
public class InicioFXMLController implements Initializable {

   
    @FXML
    private ComboBox combiop;
    @FXML
    private Button bt;
    private Cliente c;
    @FXML
    private Text id1;
    @FXML
    private GridPane gridpan;
    static String ID;
    /**
     * Initializes the controller class.
     */
    
    
  
    @Override
 
    public void initialize(URL url, ResourceBundle rb) {

        ArrayList<String> opciones= new ArrayList<>();
        opciones.add("Configuraciones");
        opciones.add("Cerrar Sesion");
        combiop.setItems(FXCollections.observableArrayList(opciones));
        setId(ID);

        
  
    }  

    public void setId(String u) {
       id1.setText(u);
    }
   

    @FXML
    private void mostrarOpciones(ActionEvent event) throws Exception {
        ComboBox opc= (ComboBox)event.getSource();
        String opciones= String.valueOf(opc.getValue());
        switch(opciones){
            case "Configuraciones":
                try {
                    FXMLLoader fxmloader= App.loadFXMLLoad("OpcionesFXML");
                    App.setRoot(fxmloader);
                    
                } catch (IOException ex) {
                    ex.printStackTrace();}
                break;
            case "Cerrar Sesion":
                Stage s= (Stage)opc.getScene().getWindow();
                s.close();
                break;
        }

    }
    @FXML
    private void btver(ActionEvent event) {
        try {
            id1.setText(ID); 
            Cliente cli=Util.obtenerPersona(id1.getText(), "personas.ser");
            String rol= cli.getCuenta().getTipoCuenta();
            switch(rol){
                case "Comprador":
                    accionNavegar();        
                    break;
                case "Vendedor":
                    accionIngV(); revO();
                    break;
                case "Ambos":
                    accionNavegar(); accionIngV(); revO();
                    break;
            }} catch (Exception ex) {
            System.out.println(ex.getMessage());}
    }
    private void accionNavegar() throws FileNotFoundException{
        Button bnavegar= new Button("Navegar");
        bnavegar.setLayoutX(30);
        Image image = new Image(new FileInputStream("imagenes\\buscar.jpg"));
        ImageView imageView1 = new ImageView(image);
        imageView1.setFitHeight(150);
        imageView1.setFitWidth(150); 
        gridpan.add(imageView1, 1, 0);
        gridpan.add(bnavegar, 1, 1);
        bnavegar.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent t) {
            try {
                NavegarFXMLController.identificacion= id1.getText();
                FXMLLoader fxml= App.loadFXMLLoad("NavegarFXML");
                App.setRoot(fxml);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
                        
            } 
        });}
        private void accionIngV() throws FileNotFoundException{
        
        Button bninV= new Button("Ingresar Vehículo");
        Image image = new Image(new FileInputStream("imagenes\\Ingresar.jpg"));
        ImageView imageView1 = new ImageView(image);
        imageView1.setFitHeight(150);
        imageView1.setFitWidth(150); 
        gridpan.add(imageView1, 2, 0);
        gridpan.add(bninV, 2, 1);
        bninV.setOnAction(new EventHandler<ActionEvent>(){
        @Override
        public void handle(ActionEvent t) {

            try {
            VehiculoFXMLController.Vendedor= id1.getText();
            FXMLLoader fxml= App.loadFXMLLoad("VehiculoFXML");
            App.setRoot(fxml);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } 
        });

    }
        private void revO() throws FileNotFoundException{
        Button bnRevisarO= new Button("Revisar Ofertas");
        Image image = new Image(new FileInputStream("imagenes\\publicacion.jpg"));
        ImageView imageView1 = new ImageView(image);
        imageView1.setFitHeight(150);
        imageView1.setFitWidth(150); 
        gridpan.add(imageView1, 0, 0);
        gridpan.add(bnRevisarO, 0, 1);
        bnRevisarO.setOnAction(new EventHandler<ActionEvent>(){
        @Override
        public void handle(ActionEvent t) {
            try {
            NavegarFXMLController.identificacion= id1.getText();
            FXMLLoader fxml= App.loadFXMLLoad("MisVentasFXML");
            App.setRoot(fxml);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        
        } 
        });
    
    }
    private void buscarOfertas(ActionEvent event) {
        try {
            FXMLLoader fxmloader= App.loadFXMLLoad("NavegarFXML");
            App.setRoot(fxmloader);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }

    private void llevarMiVenta(ActionEvent event) {
         try {
            FXMLLoader fxmloader= App.loadFXMLLoad("MisVentasFXML");
            App.setRoot(fxmloader);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void verInformacion(MouseEvent event) {
        try {
           FXMLLoader  fxm = App.loadFXMLLoad("InformacionFXML");
           App.setRoot(fxm);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
