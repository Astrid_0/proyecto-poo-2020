/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cuenta;
import ec.edu.espol.model.Cliente;
import ec.edu.espol.util.Util;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author DELL i5
 */
public class RegistrarseFXMLController implements Initializable {


    @FXML
    private TextField nombres;
    @FXML
    private TextField apellidos;
    @FXML
    private TextField correo;
    @FXML
    private TextField organizacion;
    @FXML
    private TextField usuario;
    @FXML
    private TextField clave;
    @FXML
    private ComboBox rol;
    @FXML
    private Button btnregistrar;
    @FXML
    private Button botvolver;
    /**
     * Initializes the controller class.
     */
    
    
      
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> roles= new ArrayList<>();
        roles.add("Seleccionar");
        roles.add("Comprador");
        roles.add("Vendedor");
        roles.add("Ambos");
        rol.setItems(FXCollections.observableArrayList(roles));
        rol.getSelectionModel().selectFirst();
         
    }    

    @FXML
    private void registrar(MouseEvent event) {
        String rolE= (String)rol.getValue();
        if(nombres.getText().isEmpty()||apellidos.getText().isEmpty()||correo.getText().isEmpty()||usuario.getText().isEmpty()
           ||clave.getText().isEmpty()||organizacion.getText().isEmpty()||rolE.equals("Seleccionar")){
            Alert alerta= new Alert(Alert.AlertType.INFORMATION,"No ha ingresado la informacion");
            alerta.show();
        }
        else{
            if(Util.validarUsuario(usuario.getText(), "personas.ser")){
                Alert alerta2= new Alert(Alert.AlertType.INFORMATION,"El usuario que está ingresando ya existe");
                alerta2.show();
            }
            else if(Util.validarCorreo(correo.getText(), "personas.ser")){
                Alert alerta3= new Alert(Alert.AlertType.INFORMATION,"El correo que está ingresando ya está en uso");
                alerta3.show();
            }
            else if(!Util.isLetras((String)nombres.getText()) || !Util.isLetras((String)apellidos.getText())){
                Alert alerta4= new Alert(Alert.AlertType.INFORMATION,"Nombre y Apellido no deben poseer caracteres numéricos");
                alerta4.show();
            }
            else{
                
                Cliente p= new Cliente(nombres.getText(),apellidos.getText(),new Cuenta(correo.getText(),organizacion.getText(),usuario.getText(),clave.getText(),rolE));
                ArrayList<Cliente> personas= Util.deserealizarPersonas("personas.ser");
                personas.add(p);
                Util.serializarPersonas(personas, "personas.ser");
                Alert alerta4= new Alert(Alert.AlertType.CONFIRMATION,"Registro exitoso");
                alerta4.show();
                try {
                    FXMLLoader fxmlLoader= App.loadFXMLLoad("LoginFXML");
                    App.setRoot(fxmlLoader);
                } 
                catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

    @FXML
    private void regresar(ActionEvent event) {
       
        try {
            FXMLLoader fxml= App.loadFXMLLoad("LoginFXML");
            App.setRoot(fxml) ;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        
    }
    
}
