/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cliente;
import ec.edu.espol.util.Hash;
import ec.edu.espol.util.Util;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
/**
 * FXML Controller class
 *
 * @author ronal
 */
public class OpcionesFXMLController implements Initializable {


    @FXML
    private Button btnvolver;
    @FXML
    private GridPane gridpane;
    @FXML
    private VBox vbox;
    static String user;
     TextField oldpass= new TextField();
     TextField newpass= new TextField();
      ComboBox cob= new ComboBox();
       private String roln;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    

    @FXML
    private void volver(ActionEvent event) {
        
        try {
            FXMLLoader fx = App.loadFXMLLoad("InicioFXML");
            App.setRoot(fx);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
      
    }

    @FXML
    private void cambiarPassword(ActionEvent event) {
        vbox.getChildren().clear();
       
        vbox.getChildren().addAll(new Text("Ingrese su contraseña anterior"),oldpass,new Text("Ingrese la nueva contraseña"), newpass);
      
    }

    @FXML
    private void editarRol(ActionEvent event) {
        ArrayList<String> roles= new ArrayList<>();
        roles.add("Seleccionar");
        roles.add("Comprador");
        roles.add("Vendedor");
        roles.add("Ambos");
        cob.setItems(FXCollections.observableArrayList(roles));
        cob.getSelectionModel().selectFirst();
        gridpane.add(cob, 2, 1);
    }
@FXML
    private void guardarCambios(ActionEvent event) {
        System.out.println("CONTRASEÑAS-------------------");
        String op=oldpass.getText();
        System.out.println(op);
        try {
            String op1=Hash.EncriptarPassword(op);
            System.out.println("CLAVE 2");
            Cliente c=Util.obtenerPersona(user,"personas.ser");
            String contra=c.getCuenta().getClave();
            System.out.println(contra);
            
            roln=String.valueOf(cob.getValue());
            if(!op1.equals(contra)){
               
            Alert alerta= new Alert(Alert.AlertType.INFORMATION,"La contraseña actual incorrecta");
                alerta.show();
            }else if(roln.equals("Seleccionar")){
                Alert alerta= new Alert(Alert.AlertType.INFORMATION,"Escoja un rol");
                alerta.show();
            }else{
            
            ArrayList<Cliente> personas= Util.deserealizarPersonas("personas.ser");
            for(Cliente cl:personas){
                if ((cl.getCuenta().getUsuario()).equals(c.getCuenta().getUsuario())){
                cl.getCuenta().setTipoCuenta(roln);
                cl.getCuenta().setClave(newpass.getText());
                }
            }
            Util.serializarPersonas(personas, "personas.ser");
            FXMLLoader fxmlLoader= App.loadFXMLLoad("InicioFXML");
            App.setRoot(fxmlLoader);
            
            }} catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        
    }
}
