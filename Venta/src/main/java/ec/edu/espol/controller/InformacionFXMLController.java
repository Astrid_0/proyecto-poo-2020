/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cliente;
import ec.edu.espol.util.Util;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Hogar
 */
public class InformacionFXMLController implements Initializable {

    @FXML
    private Text nombres;
    @FXML
    private Text apellidos;
    @FXML
    private Text correo;
    @FXML
    private Text usuario;
    @FXML
    private Text organizacion;
    @FXML
    private Text rol;
    private String usuariog;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuariog= InicioFXMLController.ID;
        try {Cliente c= Util.obtenerPersona(usuariog, "personas.ser");
            this.nombres.setText(c.getNombre());
            this.apellidos.setText(c.getApellido());
            this.correo.setText(c.getCuenta().getCorreoElectronico());
            this.organizacion.setText(c.getCuenta().getOrganizacion());
            this.usuario.setText(c.getCuenta().getUsuario());
            this.rol.setText(c.getCuenta().getTipoCuenta());
        } catch (Exception ex) {
            ex.printStackTrace();
        }      
    }    

    @FXML
    private void regresar(ActionEvent event) {
        try {
           FXMLLoader  fxm = App.loadFXMLLoad("InicioFXML");
           App.setRoot(fxm);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }
    
}
