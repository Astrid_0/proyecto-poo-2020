/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import static ec.edu.espol.controller.NavegarFXMLController.ofertaV;
import ec.edu.espol.model.Cliente;
import ec.edu.espol.model.Venta;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Hogar
 */
public class OfertaPrecioFXMLController implements Initializable {

    @FXML
    private TextField txPrecio;
    static Cliente cli;
    static Venta v;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        txPrecio.setTextFormatter(new TextFormatter<>(change->(change.getControlNewText().matches("([1.0-9.0][0-9.0]*)?"))?change:null));
    }    

    @FXML
    private void aceptarPrecio(ActionEvent event) {
        Double valorOferta;
        try{
            valorOferta= Double.parseDouble(txPrecio.getText());
            Button opc= (Button)event.getSource();
            Stage s= (Stage)opc.getScene().getWindow();
            if(txPrecio.getText().isEmpty() || valorOferta>=0 && valorOferta<1){
            Alert a= new Alert(Alert.AlertType.ERROR, "Ingrese valor mayor adecuado para el automovil");
            a.show();
            }
            else{
                NavegarFXMLController.ofertaV=valorOferta;
                NavegarFXMLController.venta=v;
                cli.ofertar(ofertaV,  v);
                s.close();
            }

       }
       catch(NumberFormatException ex){
           Alert a= new Alert(Alert.AlertType.ERROR,"Ingrese un valor de oferta");
           a.show();
       }
       
    }
   
    
}
