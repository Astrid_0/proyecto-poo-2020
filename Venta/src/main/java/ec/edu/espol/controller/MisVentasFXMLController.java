/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cliente;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Venta;
import ec.edu.espol.util.Util;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;


/**
 * FXML Controller class
 *
 * @author ronal
 */
public class MisVentasFXMLController implements Initializable {

    @FXML
    private VBox misVehiculos;
    private String usuariog;
    @FXML
    private  VBox ofertas;
    @FXML
    private ScrollPane scr2;
    @FXML
    private ScrollPane scr1;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuariog= InicioFXMLController.ID;
        try {Cliente c= Util.obtenerPersona(usuariog, "personas.ser");
            ArrayList<Venta> misVentas= c.obtenerMisVentas();
            if(misVentas.isEmpty())misVehiculos.getChildren().add(new Text("No presenta vehículos registrados"));
            for(Venta v: misVentas){Text dato= new Text("Marca: "+v.getVehiculo().getMarca()+", Modelo: "+v.getVehiculo().getModelo()+", Placa: "+v.getVehiculo().getPlaca()+", Precio: $"+v.getVehiculo().getPrecio());
                dato.setOnMouseClicked(new EventHandler<MouseEvent>(){
                        @Override public void handle(MouseEvent arg0){
                            ArrayList<Oferta> misOfertas= Util.ofertasOrdenadas(v.getMisOfertas());ofertas.getChildren().clear();
                            if(misOfertas.isEmpty())ofertas.getChildren().add(new Text("No presenta ofertas para su vehículo"));
                            for(Oferta o: misOfertas){
                                Button bn= new Button("Aceptar oferta");
                                ofertas.getChildren().addAll(new Text(o.toString()),bn);ofertas.setSpacing(10); 
                                bn.setOnMouseClicked(new EventHandler<MouseEvent>(){
                                        @Override public void handle(MouseEvent arg0){
                                            Hilo h= new Hilo(o,"correoVentas.properties");
                                            seteoVentas(v);confirmacion();h.start();}
                                   });
                            }
                        }
                    });misVehiculos.getChildren().add(dato);misVehiculos.setSpacing(10);
            } 
        }catch(Exception ex) {}
    }    

    @FXML
    private void volver(ActionEvent event) {
        try {
            FXMLLoader fxml= App.loadFXMLLoad("InicioFXML");
            App.setRoot(fxml);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private void seteoVentas(Venta venta){
        ArrayList<Venta> nuevasVentas= Util.deserealizarVentas("ventas.ser");
        for (int i= 0; i < nuevasVentas.size(); i++) {
            if(venta.getVehiculo().getPlaca().equals(nuevasVentas.get(i).getVehiculo().getPlaca()))
                nuevasVentas.remove(nuevasVentas.indexOf(nuevasVentas.get(i)));
        }
        Util.serializarVentas(nuevasVentas, "ventas.ser");
    }
    
    private void confirmacion(){
        try {Alert a= new Alert(Alert.AlertType.CONFIRMATION,"Ha aceptado la oferta");
            a.show();
            FXMLLoader fxml= App.loadFXMLLoad("InicioFXML");
            App.setRoot(fxml);
        }catch (IOException ex) {ex.printStackTrace();}  
    }
    public class Hilo extends Thread{
        Oferta o;
        String archivo;

        public Hilo(Oferta o, String archivo) {
            this.o = o;
            this.archivo = archivo;
        }

        public Oferta getO() {
            return o;
        }

        public String getArchivo() {
            return archivo;
        }
        @Override
        public void run (){
            Util.enviarCorreo(o, archivo);
        
        }
        
    
    
    }
}
