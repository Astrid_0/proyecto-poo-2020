/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.util.Hash;
import ec.edu.espol.util.Util;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author DELL i5
 */
public class LoginFXMLController implements Initializable {


    @FXML
    private Button login;
    @FXML
    private Button registrarse;
    @FXML
    private TextField usuario;
    @FXML
    private TextField contraseña;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }    


    @FXML
    private void login(MouseEvent event) {

        try {if(usuario.getText().isEmpty()||contraseña.getText().isEmpty()){
                Alert alerta= new Alert(Alert.AlertType.INFORMATION,"No se ha ingresado la información");
                alerta.show();
            }
            else{
                if(Util.usuarioValido(usuario.getText(),Hash.EncriptarPassword(contraseña.getText()),"personas.ser")){
                        InicioFXMLController.ID=usuario.getText();
                        FXMLLoader fxmlLoader= App.loadFXMLLoad("InicioFXML");
                        App.setRoot(fxmlLoader);
                        InicioFXMLController inicio= fxmlLoader.getController();
                        OpcionesFXMLController.user=usuario.getText();

                }
                else{
                    Alert alerta2= new Alert(Alert.AlertType.INFORMATION,"Verifique su usuario y contraseña");
                    alerta2.show();
                }
            }
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void registrarse(MouseEvent event) {
        try {
            FXMLLoader fxmlLoader= App.loadFXMLLoad("RegistrarseFXML");
            App.setRoot(fxmlLoader);
        } 
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public TextField getUsuario() {
        return usuario;
    }

}
