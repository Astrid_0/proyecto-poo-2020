/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.main;

import espol.edu.ec.common.Camion;
import espol.edu.ec.common.Camioneta;
import espol.edu.ec.common.Carro;
import espol.edu.ec.common.Comprador;
import espol.edu.ec.common.Persona;
import espol.edu.ec.common.Motocicleta;
import espol.edu.ec.common.Oferta;
import espol.edu.ec.common.Vehiculo;
import espol.edu.ec.common.Vendedor;
import espol.edu.ec.common.Venta;
import espol.edu.ec.util.Hash;
import java.util.Scanner;
import espol.edu.ec.util.Util;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hogar
 */
public class Main {
    public static void main( String[] args){
        ArrayList<Comprador> compradores= new ArrayList<>();
        Scanner sc= new Scanner(System.in);
        sc.useDelimiter("\n");
        String opcion;
        do{
        do{
            System.out.println("Menu de opciones");
            System.out.println("1. Vendedor\n2. Comprador\n3. Salir");
            opcion=sc.next();}while(!(Util.isDigit(opcion)));
        
            switch(Integer.valueOf(opcion)){
                case 1:
                    String opcion1;
                    do{
                        System.out.println("1.Registrar un nuevo vendedor\n2.Ingresar un nuevo vehículo\n3.Aceptar oferta\n4.Regresar");
                        opcion1=sc.next();
                        switch(Integer.valueOf(opcion1)){
                            case 1:
                                System.out.println("Ingrese su nombre:");
                                String nombre= sc.next();
                                System.out.println("Ingrese sus apellidos");
                                String apellidos= sc.next();
                                System.out.println("Nota:\n1. Si su correo electronico ya está registrado se le volverá a solicitar\n2.Si el usuario que desa crear ya existe, ingrese uno nuevo");                 
                                String correoE;
                                do{
                                    System.out.println("Ingrese su correo electronico");
                                    correoE= sc.next();
                                }while(Util.validarCorreo(correoE,"vendedor.txt"));
                                System.out.println("Ingrese su organizacion");
                                String organizacion= sc.next();
                                String usuario;
                                do{
                                    System.out.println("Ingrese el usuario que desea, si este ya existe se le pedirá otro ingreso");
                                    usuario= sc.next();
                                }while(Util.validarUsuario(usuario,"vendedor.txt"));
                                System.out.println("Ingresa su contraseña");
                                String contrasenia= sc.next();
                                Persona p=new Vendedor(nombre,apellidos,correoE,organizacion,usuario,contrasenia);
                                Vendedor vendedor= (Vendedor)p;
                                vendedor.registrarVendedor();
                            break;
                                //
                            case 2:
                                String usuarioN;
                                do{
                                    System.out.println("Para acceder a esta sección debe ingresar con su usuario \nIngrese su usuario");
                                    usuarioN= sc.next();
                                    if(!Util.validarUsuario(usuarioN,"vendedor.txt"))
                                        System.out.println("Usuario de vendedor no existe en el sistema");
                                }while(!Util.validarUsuario(usuarioN,"vendedor.txt"));
                                String contraseñaN;
                                do{
                                    System.out.println("Ingresa su contraseña");
                                    contraseñaN= sc.next();
                                    if(!Util.validarIngreso( usuarioN,contraseñaN,"vendedor.txt"))
                                        System.out.println("Contraseña incorrecta");
                                }while(!Util.validarIngreso(usuarioN,contraseñaN,"vendedor.txt"));
                                Persona pe= Util.armarPersonaV(usuarioN,"vendedor.txt");
                                p=pe;
                                Vendedor vendedorN=(Vendedor)p;
                                String tipo;
                                do{
                                    System.out.println("Ingrese el tipo de vehículo (carro-camion-camioneta-motocicleta)");
                                    tipo= sc.next().toLowerCase();
                                }while(!Util.tipoVehiculoValido(tipo));
                                String placa;
                                do{
                                    System.out.println("Ingrese la placa de su vehículo");
                                    System.out.println("Nota: Si la placa no es válida o ya existe en el sistema se le solicitará nuevo ingreso");
                                    placa= sc.next().toUpperCase();
                                    if(!Util.placaValida(placa))
                                        System.out.println("Placa no válida");
                                    else if(!Util.placaExistente(placa))
                                        System.out.println("Placa ya existente");
                                }while(!Util.placaValida(placa)&&!Util.placaExistente(placa));  
                                System.out.println("Ingrese la marca de su vehículo");
                                String marca= sc.next();
                                System.out.println("Ingrese el modelo de su vehículo");
                                String modelo= sc.next().toLowerCase();
                                System.out.println("Ingrese el tipo de motor de su vehículo");
                                String tipoMotor= sc.next().toLowerCase();
                                String año;
                                do{
                                    System.out.println("Ingrese el año de su vehículo (Sin separadores de miles)");
                                    año= sc.nextLine();
                                }while(Util.isDigit(año));
                                System.out.println("Ingrese el color de su vehículo");
                                String color= sc.next().toLowerCase();
                                String combustible;
                                do{
                                    System.out.println("Ingrese el tipo de combustible (gasolina-diesel-electricidad)");
                                    combustible= sc.next().toLowerCase();
                                }while(!Util.combustibleValido(combustible));
                                System.out.println("Ingrese la transmisión de su vehículo");
                                String transmision= sc.next().toLowerCase();
                                double recorrido;
                                do{
                                    System.out.println("Ingrese el recorrido de su vehículo");
                                    recorrido= sc.nextDouble();
                                }while(recorrido<0);
                                double precio;
                                do{
                                    System.out.println("Ingrese el precio de su vehículo ");
                                    precio= sc.nextDouble();
                                }while(precio<0);
                                switch(tipo){
                                    case "carro":
                                        String vidrio;
                                        do{
                                            System.out.println("Ingrese el tipo de vidrios de su vehículo (manual-automático)");
                                            vidrio= sc.next().toLowerCase();
                                        }while(!Util.vidrioValido(vidrio));
                                        int asientosNiños;
                                        do{
                                            System.out.println("Ingrese el número de sisentos para niños que posee su vehículo");
                                            asientosNiños= sc.nextInt();
                                        }while(asientosNiños<0);
                                        Vehiculo carro= new Carro(placa,marca,modelo,tipoMotor,año,color,vidrio,combustible,transmision,recorrido,precio,asientosNiños);
                                        ArrayList<Oferta> misOfertasC= new ArrayList<>();
                                        Venta ventaC= new Venta(carro,vendedorN,misOfertasC);
                                        vendedorN.ingresarVehiculo(ventaC);
                                    break;
                                    case "camion":
                                        String vidrio2;
                                        do{
                                            System.out.println("Ingrese el tipo de vidrios de su vehículo (manual-automático)");
                                            vidrio2= sc.next().toLowerCase();
                                        }while(!Util.vidrioValido(vidrio2));
                                        double carga;
                                        do{
                                            System.out.println("Ingrese la capacidad de carga de su vehículo");
                                            carga= sc.nextDouble();
                                        }while(carga<0);
                                        Vehiculo camion= new Camion(placa,marca,modelo,tipoMotor,año,color,vidrio2,combustible,transmision,recorrido,precio,carga);
                                        ArrayList<Oferta> misOfertasCm= new ArrayList<>();
                                        Venta ventaCm= new Venta(camion,vendedorN,misOfertasCm);
                                        vendedorN.ingresarVehiculo(ventaCm);
                                    break;
                                    case "camioneta":
                                        String vidrio3;
                                        do{
                                            System.out.println("Ingrese el tipo de vidrios de su vehículo (manual-automático)");
                                            vidrio3= sc.next().toLowerCase();
                                        }while(!Util.vidrioValido(vidrio3));
                                        String traccion;
                                        do{
                                            System.out.println("Ingrese el tipo de traccion de su vehículo (delantera-trasera-integral)");
                                            traccion= sc.next().toLowerCase();
                                        }while(!Util.traccionValida(traccion));
                                        Vehiculo camioneta= new Camioneta(placa,marca,modelo,tipoMotor,año,color,vidrio3,combustible,transmision,recorrido,precio,traccion);
                                        ArrayList<Oferta> misOfertasCmt= new ArrayList<>();
                                        Venta ventaCmt= new Venta(camioneta,vendedorN,misOfertasCmt);
                                        vendedorN.ingresarVehiculo(ventaCmt); 
                                    break;
                                    case "motocicleta":
                                        System.out.println("Ingrese el tipo de moto que es su vehículo");
                                        String tipoMoto= sc.next().toLowerCase();
                                        System.out.println("Ingrese el tipo de refrigeración de su vehículo");
                                        String refrigeracion= sc.next().toLowerCase();
                                        Vehiculo motocicleta= new Motocicleta(placa,marca,modelo,tipoMotor,año,color,combustible,transmision,recorrido,precio,tipoMoto,refrigeracion);
                                        ArrayList<Oferta> misOfertasM= new ArrayList<>();
                                        Venta ventaM= new Venta(motocicleta,vendedorN,misOfertasM);
                                        vendedorN.ingresarVehiculo(ventaM); 
                                    break;
                                }
                            break;

                            //
                            case 3:
                                String usuarioN2;
                                do{
                                    System.out.println("Para acceder a esta sección debe ingresar con su usuario \nIngrese su usuario");
                                    usuarioN2= sc.next();
                                    if(!Util.validarUsuario(usuarioN2,"vendedor.txt"))
                                        System.out.println("Usuario de vendedor no existe en el sistema");
                                }while(!Util.validarUsuario(usuarioN2,"vendedor.txt"));
                                String contraseñaN2;
                                do{
                                    System.out.println("Ingresa su contraseña");
                                    contraseñaN2= sc.next();
                                    if(!Util.validarIngreso(usuarioN2, contraseñaN2,"vendedor.txt"))
                                        System.out.println("Contraseña incorrecta");
                                }while(!Util.validarIngreso(usuarioN2, contraseñaN2,"vendedor.txt"));
                                Persona pe2= Util.armarPersonaV(usuarioN2,"vendedor.txt");
                                p=pe2;
                                Vendedor vendedorN2=(Vendedor)p;
                                String placaC;
                                do{
                                    System.out.println("Ingrese la placa de su vehículo");
                                    System.out.println("Nota: Si la placa no es válida o no existe en el sistema se le solicitará nuevo ingreso");
                                    placaC= sc.next().toUpperCase();
                                    if(!Util.placaValida(placaC))
                                        System.out.println("Placa no válida");
                                    else if(!Util.placaExistente(placaC))
                                        System.out.println("Placa no existente");
                                }while(!Util.placaValida(placaC)||!Util.placaExistente(placaC));
                                ArrayList<Oferta> misOfertas= vendedorN2.revisarOfertas(placaC);
                                String opcionC;
                                int ii=0;
                                try{
                                    do{
                                        System.out.println("Oferta: "+(ii+1));
                                        System.out.println(misOfertas.get(ii));
                                        if(ii==0){
                                            System.out.println("(s) Para avanzar\n(a) Para aceptar oferta\n(e) Para salir");
                                            opcionC= sc.next().toLowerCase(); 
                                            if(opcionC.equals("s"))
                                                ii++;
                                            else if(opcionC.equals("a")){
                                               // Util.eliminar("vehiculosEnVenta.txt",misOfertas.get(ii).getVenta(),Vehiculo.vehiculosEnVenta);
                                                Util.enviarCorreo(misOfertas.get(ii).getComprador(),misOfertas.get(ii).getVenta());
                                            }
                                        }
                                        else if(ii==misOfertas.size()-1){
                                            System.out.println("(r) Para regresar\n(a) Para aceptar oferta\n(e) Para salir");
                                            opcionC= sc.next().toLowerCase(); 
                                            if(opcionC.equals("r"))
                                                ii--;
                                            else if(opcionC.equals("a")){
                                            //    Util.eliminar("vehiculosEnVenta.txt",misOfertas.get(ii).getVenta(),Vehiculo.vehiculosEnVenta);
                                                Util.enviarCorreo(misOfertas.get(ii).getComprador(),misOfertas.get(ii).getVenta());
                                            }
                                        }
                                        else{
                                            System.out.println("(s) Para avanzar\n(r) Para regresar\n(a) Para aceptar oferta\n(e) Para salir");
                                        opcionC= sc.next().toLowerCase(); 
                                            if(opcionC.equals("s"))
                                                ii++;
                                            else if(opcionC.equals("r"))
                                                ii--;
                                            else if(opcionC.equals("a")){
                                              //  Util.eliminar("vehiculosEnVenta.txt",misOfertas.get(ii).getVenta(),Vehiculo.vehiculosEnVenta);
                                                Util.enviarCorreo(misOfertas.get(ii).getComprador(),misOfertas.get(ii).getVenta());
                                            }
                                        }

                                    }while(!opcionC.equals("e")&&!opcionC.equals("a"));  
                                }
                                catch(Exception e){}
                            break;   
                        } 
                    }while(Util.isDigit(opcion1)&&Integer.valueOf(opcion)!=4);
                break;
                //comprador
                case 2:
             
                    String opcionc;
                    
                    do{
                        System.out.println("1.Reistrar un nuevo comprador\n2.Ofertar por un vehiculo");
                        opcionc= sc.next();
                        }while (!(Util.isDigit(opcionc)));
                        switch(Integer.valueOf(opcionc)){
                            
                                
                            case  1:
                                Comprador c= Comprador.leerTecladoComprador();
                                compradores.add(c);
                                
                                break;
                            case 2:
                                try{
                               
                                String user;
                                String password;
                                do{
                                    System.out.println("Usuario:");
                                    user= sc.next();
                                    System.out.println("Password");
                                    password=sc.next();
                                }while(!Util.validarIngreso(user, password,compradores));
                                
                                for(Comprador co: compradores){
                                    if(co.getCuenta().getUsuario().equals(user)){
                                        String respuesta;
                                        
                                        do{
                                            System.out.println("¿Desea ingresar criterios de búsqueda S/N?");
                                            respuesta=sc.next();
                                        }while(!(respuesta.equals("S")||respuesta.equals("N")));
                                        switch (respuesta){
                                        case "S":
                                            String criterio;
                                            System.out.println("Seleccione un criterio:");
                                            do{
                                                System.out.println("1.Tipo de vehículo\n2.Recorrido\n3.Año\n4.Precio");
                                                criterio=sc.next();
                                            }while(!(Util.isDigit(criterio)));
                                            switch(Integer.valueOf(criterio)){
                                        
                                                case 1:
   
                                                    String s;
                                                    System.out.println("MOTOCICLETAS");
                                                    ArrayList<Venta> motos= Vehiculo.filtrarMotos(Vehiculo.vehiculosEnVenta);
                                                    Comprador.presentarVehiculo(motos, co);
                                                    System.out.println("-----------------------------------------------------------------------------");

                                                    System.out.println("CARROS");
                                                    ArrayList<Venta> carros= Vehiculo.filtrarCarros(Vehiculo.vehiculosEnVenta);
                                                    Comprador.presentarVehiculo(carros, co);


                                                    System.out.println("------------------------------------------------------------------------------");
                                                    System.out.println("CAMIONES");
                                                    ArrayList<Venta> camiones= Vehiculo.filtrarCamion(Vehiculo.vehiculosEnVenta);
                                                    Comprador.presentarVehiculo(carros, co);


                                                    System.out.println("--------------------------------------------------------------------------------");
                                                    System.out.println("CAMIONETAS");
                                                    
                                                    ArrayList<Venta> camionetas= Vehiculo.filtrarCamionetas(Vehiculo.vehiculosEnVenta);
                                                    Comprador.presentarVehiculo(camionetas, co);

                                                    System.out.println("---------------------------------------------------------------------------------");
                                                    break;
                                                case 2:
                                                    System.out.println("Bienvenido a la búsqueda por recorrido, a continuacion ingrese los parámetros para la búsqueda");
                                                    String rangoi;
                                                    String rangof;

                                                    do{

                                                        System.out.println("Desde:");
                                                        rangoi=sc.next();
                                                        System.out.println("Hasta:");
                                                        rangof=sc.next();


                                                    }while(!(Util.isDigit(rangoi) && Util.isDigit(rangof)));
                                                    ArrayList<Venta> vehiculos= Vehiculo.filtrarRecorrido(Double.parseDouble(rangoi),Double.parseDouble(rangof));

                                                    Comprador.presentarVehiculo(vehiculos, co);
                                                    break;
                                                case 3:
                                                    System.out.println("Bienvenido a la búsqueda por año, a continuacion ingrese los parámetros para la búsqueda");
                                                    String anioi;
                                                    String aniof;

                                                    do{
                                                        System.out.println("Desde:");
                                                        anioi=sc.next();
                                                        System.out.println("Hasta:");
                                                        aniof=sc.next();

                                                    }while(!(Util.isDigit(anioi) && Util.isDigit(aniof)));
                                                    ArrayList<Venta> vehiculosA= Vehiculo.filtrarAnio(Integer.parseInt(anioi),Integer.parseInt(aniof));
                                                    Comprador.presentarVehiculo(vehiculosA, co);
                                                    break;             
                                                case 4:
                                                    System.out.println("Bienvenido a la búsqueda por precio, a continuación ingrese los parámetros para la búsqueda");
                                                    String precioi;
                                                    String preciof;
                                                  
                                                        do{
                                                            System.out.println("Desde:");
                                                            precioi=sc.next();
                                                            System.out.println("Hasta:");
                                                            preciof=sc.next();

                                                        }while(!(Util.isDigit(precioi) && Util.isDigit(preciof)));
                                                        ArrayList<Venta> vehiculosP= Vehiculo.filtrarPrecio(Double.parseDouble(precioi),Double.parseDouble(preciof));
                                                        Comprador.presentarVehiculo(vehiculosP, co);
                                                    break;             
                                            }
                                        case "N":
                                            Comprador.presentarVehiculo(Vehiculo.vehiculosEnVenta, co);

                                    break;}
                                }
                        }            
                         
                        
            }
                catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } 
                            
                        
                        }
            
    
    }}while(Integer.valueOf(opcion)==1|| Integer.valueOf(opcion)==2);
        System.out.println("Gracias por usar el programa");
       

}}
        



