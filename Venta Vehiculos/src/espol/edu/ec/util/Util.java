
package espol.edu.ec.util;
import espol.edu.ec.common.Comprador;
import espol.edu.ec.common.Persona;
import espol.edu.ec.common.Vehiculo;
import espol.edu.ec.common.Vendedor;
import espol.edu.ec.common.Venta;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
public class Util {
    private Util(){    
    }
    
    public static boolean isDigit(String s){
        try{
            if(Integer.parseInt(s)>=0)
                return true;
            return false;
        }
        catch(Exception e){
            return false;
        }
    }
    //https://www.lawebdelprogramador.com/foros/Java/1680183-ingresar-solo-letras-y-no-numeros.html
    public static boolean isLetras(String s){
        String sUp= s.toUpperCase();
        int indicador=0;
        for(int i= 0; i<sUp.length();i++){
            char caracter= sUp.charAt(i);
            if(caracter>=65&&caracter<=90)
                indicador++;
        }
        if(indicador==sUp.length())
            return true;
        else
            return false;
    }
    
    public static void serializarPersonas(ArrayList<Persona> personas, String nomFile){
        try(FileOutputStream f= new FileOutputStream(nomFile);
                ObjectOutputStream out= new ObjectOutputStream(f)){
            out.writeObject(personas);
        } 
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static ArrayList<Persona> deserealizarPersonas(String nomFile){
        ArrayList<Persona> personas= new ArrayList<>();
        try(FileInputStream f= new FileInputStream(nomFile);
                ObjectInputStream in= new ObjectInputStream(f)){
            personas= (ArrayList<Persona>)in.readObject();
            return personas;
        } 
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return personas;
    }
    
     public static void serializarVentas(ArrayList<Venta> ventas, String nomFile){
        try(FileOutputStream f= new FileOutputStream(nomFile);
                ObjectOutputStream out= new ObjectOutputStream(f)){
            out.writeObject(ventas);
        } 
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
     
    public static ArrayList<Venta> deserealizarVentas(String nomFile){
        ArrayList<Venta> ventas= new ArrayList<>();
        try(FileInputStream f= new FileInputStream(nomFile);
                ObjectInputStream in= new ObjectInputStream(f)){
            ventas= (ArrayList<Venta>)in.readObject();
            return ventas;
        } 
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return ventas;
    }
    
    public static boolean validarCorreo(String correo,String nomArch){
        ArrayList<Persona> datos= Util.deserealizarPersonas(nomArch);
        for(Persona p: datos){
            String correoC= p.getCuenta().getCorreoElectronico();
            if(correoC.equals(correo)){
                return true;
            }
        }
        return false ;    
    }
    public static boolean  validarUsuario(String usuario,String nomArch){
         ArrayList<Persona> datos= Util.deserealizarPersonas(nomArch);
        for(Persona p: datos){
            String usuarioC= p.getCuenta().getUsuario();
            if(usuarioC.equals(usuario)){
                return true;
            }
        }
        return false ;}
 
   
    public static boolean placaExistente(String placa,String nomArch){
        ArrayList<Venta> listaVentas= Util.deserealizarVentas(nomArch);
        for(Venta ventaC: listaVentas){
            String placaC= ventaC.getVehiculo().getPlaca();
            if(placaC.equals(placa))
                return true;
        }    
        return false;
    }
    
    public static void enviarCorreo(Persona p, Venta venta){
        String notificación= "Se ha aceptado su oferta";
        String cuerpo= "Su oferta ha sido aceptada para: "+venta.getVehiculo();
        String remitente= "ventadevehiculos123@gmail.com";
        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.clave","0123456789ABCDEF");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(remitente));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(((Comprador)p).getCuenta().getCorreoElectronico()));   //Se podrían añadir varios de la misma manera
            message.setSubject(notificación);
            message.setText(cuerpo);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", remitente, "0123456789ABCDEF");
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me) {
            me.printStackTrace();   //Si se produce un error
        }
    }
    
    public static boolean placaValida(String placa){
        if(placa.length()==7&&Util.isLetras(placa.substring(0,3))&&Util.isDigit(placa.substring(3))){
            return true;
        }
        else if(placa.length()==6&&Util.isLetras(placa.substring(0,3))&&Util.isDigit(placa.substring(3))){
            return true;
        }
        return false;
    }
    
    public static boolean tipoVehiculoValido(String tipo){
        String tipoU= tipo.toUpperCase();
        if(tipoU.equals("CARRO")||tipoU.equals("CAMION")||tipoU.equals("CAMIONETA")||tipoU.equals("MOTOCICLETA")){
            return true;
        }
        return false;
    }
    
    public static boolean combustibleValido(String combustible){
        String combustibleU= combustible.toUpperCase();
        if(combustibleU.equals("GASOLINA")||combustibleU.equals("DIESEL")||combustibleU.equals("ELECTRICIDAD")){
            return true;
        }
        return false;
    }
    
    public static boolean vidrioValido(String vidrio){
        String vidrioU= vidrio.toUpperCase();
        if(vidrioU.equals("MANUAL")||vidrioU.equals("ELECTRICO"))
            return true;
        return false;
    }
    
    
    public static boolean traccionValida(String traccion){
        String traccionU= traccion.toUpperCase();
        if(traccionU.equals("DELANTERA")||traccionU.equals("TRASERA")||traccionU.equals("INTEGRAL"))
            return true;
        return false;
    }
    public static boolean validarIngreso(String user, String password, ArrayList<Comprador> compradores) throws NoSuchAlgorithmException{
        String cn="";
            for(Comprador e: compradores){
                if(e.getCuenta().getUsuario().equals(user)){
                    cn= e.getCuenta().getClave();
                }
                                 
            }
            for(Comprador e: compradores){
                System.out.println(e);
            }
        String cv=Hash.EncriptarPassword(password);
        System.out.println(cv);
        System.out.println(cn);
        return cn.equals(cv);}
}
