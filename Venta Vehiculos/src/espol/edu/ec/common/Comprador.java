/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;


import espol.edu.ec.util.Util;
import java.util.ArrayList;
import java.util.Scanner;


/**
 *
 * @author Hogar
 */
public class Comprador extends Persona{
    protected ArrayList<Oferta> ofertas= new ArrayList<>();
    private static final long serialVersionUID= 8999612343675729876L;

    public Comprador(String nombres, String apellidos, String correoE, String organizacion, String usuario, String clave) {
        super(nombres, apellidos,new Cuenta(correoE, organizacion, usuario, clave));
    }
    public ArrayList<Oferta> getOfertas() {
        return ofertas;
    }

    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }
    public static Comprador leerTecladoComprador(){
        Scanner sc= new Scanner(System.in);
        
        System.out.println("Ingrese su nombre:");
        String nombre= sc.next();
        
        System.out.println("Ingrese sus apellidos");
        String apellidos= sc.next();
        
        System.out.println("Nota:\n1. Si su correo electronico ya está registrado se le volverá a solicitar\n2.Si el usuario que desa crear ya existe, ingrese uno nuevo");                 
        String correoE;
            //do{
                System.out.println("Ingrese su correo electronico");
                correoE= sc.next();
            //}while(Util.validarCorreo(correoE,"comprador.txt"));
        
        System.out.println("Ingrese su organizacion");
        String organizacion= sc.next();
                            
        String usuario;
            //do{
                System.out.println("Ingrese el usuario que desea, si este ya existe se le pedirá otro ingreso");
                usuario= sc.next();
            //}while(Util.validarUsuario(usuario,"comprador.txt"));
       
        System.out.println("Ingrese su contraseña");
        String contrasenia= sc.next();
        //Hash.EncriptarPassword(contrasenia, usuario);
        Comprador c= new Comprador(nombre,apellidos,correoE,organizacion,usuario,contrasenia);
       
        return c;
    
    }
     public void ofertar(Venta v){
        Scanner sc= new Scanner(System.in);
        double oferta;
        System.out.println("Ingrese valor a ofertar");
        oferta=sc.nextDouble();
        v.misOfertas.add(new Oferta(oferta,this));
        
    
    }
      public static void presentarVehiculo(ArrayList ventas, Comprador c){
        int i=0;
        Scanner sc= new Scanner(System.in);
        String s;
        do{
        try{
            System.out.println(ventas.get(i));
            System.out.println("Escriba \"n\" para avanzar o  \"r\" para regresar u \"o\" para ofertar");
            s= sc.next();
            if(s.equals("n")) i++;
                                                            
            else if(s.equals("r")) i--;
            else if(s.equals("o")){
            try{
                c.ofertar((Venta)ventas.get(i));
                i++;
            }
            catch(Exception e){
                System.out.println("Valor no válido");
            }
            }}
        catch(ArrayIndexOutOfBoundsException e){
            i++;
        }
        }while(i<ventas.size());
    }
   



    }
