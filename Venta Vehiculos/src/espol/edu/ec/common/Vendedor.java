
package espol.edu.ec.common;

import espol.edu.ec.util.Util;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Vendedor extends Persona {
    private ArrayList<Venta> ventas= new ArrayList<>();
    private static final long serialVersionUID= 8999612343675728838L;
    
    public Vendedor(String nombres, String apellidos, String correoe, String organizacion, String usuario, String clave) {
        super(nombres, apellidos,new Cuenta(correoe, organizacion, usuario, clave));
    }
    
    public ArrayList<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(ArrayList<Venta> ventas) {
        this.ventas = ventas;
    }
    
    public void registrarVendedor(){
        if(!Util.validarCorreo(this.getCuenta().getCorreoElectronico(),"vendedor.ser")&&!Util.validarUsuario(this.getCuenta().getUsuario(),"vendedor.ser")){
            ArrayList<Persona> vendedores= Util.deserealizarPersonas("vendedor.ser");
            vendedores.add(this);
            Util.serializarPersonas(vendedores, "vendedor.ser");
        }
    }
    
    public void ingresarVehiculo(Venta venta){
        if(!Util.placaExistente(venta.getVehiculo().getPlaca(),"ventas.ser")){
            this.ventas.add(venta);
            ArrayList<Venta> ventas= Util.deserealizarVentas("ventas.ser");
            ventas.add(venta);
            Util.serializarVentas(ventas, "ventas.ser");
        }
    }
    
    public ArrayList<Oferta> revisarOfertas(String placa){
        ArrayList<Oferta> allofertas= new ArrayList<>();
        for(Venta ventaC: this.ventas){
            String placaC= ventaC.getVehiculo().getPlaca();
            if(placa.equals(placaC)){
                allofertas= ventaC.getOfertas();
                return allofertas;
            }   
        }
        return allofertas;
    }
    
    
    
}
