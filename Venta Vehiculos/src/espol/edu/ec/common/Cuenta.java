/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import espol.edu.ec.util.Hash;
import espol.edu.ec.util.Util;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Hogar
 */
public class Cuenta {
    private String correoElectronico;
    private String organizacion;
    private String usuario;
    private String clave;

    public Cuenta(String correoElectronico, String organizacion, String usuario, String clave) {
        this.correoElectronico = correoElectronico;
        this.organizacion = organizacion;
        this.usuario = usuario;
        this.clave = clave;
    }

    
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
       if(!Util.validarCorreo(correoElectronico,"comprador.txt"))
            this.correoElectronico = correoElectronico;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        if(!Util.validarUsuario(usuario,"comprador.txt"))
            this.usuario = usuario;
    }

    public String getClave() throws NoSuchAlgorithmException {
        return Hash.EncriptarPassword(clave);
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public String toString() {
        return "Usuario{" + ", usuario=" + usuario+ ",correoElectronico=" + correoElectronico + ", organizacion=" + organizacion+" ,password="+clave ;
    }
    
    
    
}
