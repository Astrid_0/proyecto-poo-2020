/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import espol.edu.ec.util.Util;

/**
 *
 * @author Hogar
 */
public class Camioneta extends Vehiculo{
    private String vidrio;
    private String tracción;
    
    public Camioneta(String placa, String marca, String modelo, String tipo_de_motor, String año, String color, String vidrio, 
                    String tipo_de_combustible, String transmisión, double recorrido, double precio, String tracción){
        super(placa, marca, modelo, tipo_de_motor, año, color, tipo_de_combustible, transmisión, recorrido, precio);
        this.tracción= tracción;
        this.vidrio= vidrio;
    }

    public String getVidrio() {
        return vidrio;
    }

    public void setVidrio(String vidrio) {
        if(Util.vidrioValido(vidrio))
            this.vidrio = vidrio;
    }

    public String getTracción() {
        return tracción;
    }

    public void setTracción(String tracción) {
        if(Util.traccionValida(tracción))
            this.tracción = tracción;
    }
    
    @Override
    public String toString() {
        return "Vehículo-Camioneta: Placa: "+this.placa+", Marca: "+this.marca+", Modelo: "+this.modelo+", Color: "+this.color+", Tipo de vidrio: "+this.vidrio+
                ",\nTipo de motor: "+this.tipo_de_motor+", Tipo de combustible: "+this.tipo_de_combustible+", Transmisión: "+this.transmisión+ ", Tracción: "+this.tracción+
               ", \n    Año: "+this.año+", \nRecorrido: "+this.recorrido+", Precio: "+this.precio;
    }
}

