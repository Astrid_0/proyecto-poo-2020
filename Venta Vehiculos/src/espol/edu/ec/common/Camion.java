/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import espol.edu.ec.util.Util;

/**
 *
 * @author Hogar
 */
public class Camion extends Vehiculo{
    private String vidrio;
    private double capacidad_de_carga;
    
    public Camion(String placa, String marca, String modelo, String tipo_de_motor, String año, String color, String vidrio, 
                  String tipo_de_combustible, String transmisión, double recorrido, double precio, double capacidad_de_carga){
        super(placa, marca, modelo, tipo_de_motor, año, color, tipo_de_combustible, transmisión, recorrido, precio);
        this.vidrio= vidrio;
        this.capacidad_de_carga= capacidad_de_carga;
    }

    public String getVidrio() {
        return vidrio;
    }

    public void setVidrio(String vidrio) {
        if(Util.vidrioValido(vidrio))
            this.vidrio = vidrio;
    }

    public double getCapacidad_de_carga() {
        return capacidad_de_carga;
    }

    public void setCapacidad_de_carga(double capacidad_de_carga) {
        if(capacidad_de_carga>=0)
            this.capacidad_de_carga = capacidad_de_carga;
    }
    
    @Override
    public String toString() {
        return "Vehículo-Camión \nPlaca: "+this.placa+", Marca: "+this.marca+", Modelo: "+this.modelo+", Tipo de motor: "+this.tipo_de_motor+ ", Transmisión: "+this.transmisión+
                "\nColor: "+this.color+", Tipo de combustible: "+this.tipo_de_combustible+", Recorrido: "+this.recorrido+" ,Tipo de vidio: "+this.vidrio+ 
                ",\n    Año: "+this.año+", Precio: "+this.precio+", Capacidad de carga: "+this.capacidad_de_carga;
    }
}
