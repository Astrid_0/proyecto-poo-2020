/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import java.util.ArrayList;

/**
 *
 * @author Hogar
 */
public class Venta {
    private Vehiculo vehiculo;
    private Vendedor vendedor;
    public static ArrayList<Oferta> misOfertas= new ArrayList<>();
    
    public Venta(Vehiculo vehiculo, Vendedor vendedor, ArrayList<Oferta> misOfertas){
        this.vehiculo= vehiculo;
        this.vendedor= vendedor;
        this.misOfertas= misOfertas;
    }
    
    public Vehiculo getVehiculo() {
        return vehiculo;
    }
    public Vendedor getVendedor() {
        return vendedor;
    }

    public static ArrayList<Oferta> getOfertas() {
        return misOfertas;
    }
    
  
    @Override
    public String toString(){
        return ""+this.vehiculo+", Vendedor: "+this.vendedor;
    }
}
