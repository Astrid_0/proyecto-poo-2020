/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import espol.edu.ec.util.Util;

/**
 *
 * @author Hogar
 */
public class Carro extends Vehiculo {
    private String vidrio;
    private int asientos_de_niño;
    
    public Carro(String placa, String marca, String modelo, String tipo_de_motor, String año, String color, String vidrio, 
                    String tipo_de_combustible, String transmisión, double recorrido, double precio, int asientos_de_niño){
        super(placa, marca, modelo, tipo_de_motor, año, color, tipo_de_combustible, transmisión, recorrido, precio);
        this.vidrio= vidrio;
        this.asientos_de_niño= asientos_de_niño;
    }

    public String getVidrio() {
        return vidrio;
    }

    public void setVidrio(String vidrio) {
        if(Util.vidrioValido(vidrio))
            this.vidrio = vidrio;
    }

    public int getAsientos_de_niño() {
        return asientos_de_niño;
    }

    public void setAsientos_de_niño(int asientos_de_niño) {
        if(asientos_de_niño>=0)
            this.asientos_de_niño = asientos_de_niño;
    }
    
    @Override
    public String toString(){
        return "Vehículo-Carro: Placa: "+this.placa+", Marca: "+this.marca+", Modelo: "+this.modelo+", Tipo de vidrio: "+this.vidrio+
                "\nNúmero de asientos de niño: "+this.asientos_de_niño+", Color: "+this.color+
                ",\n Tipo de motor: "+this.tipo_de_motor+", Transmisión: "+this.transmisión+", Tipo de combustible: "+this.tipo_de_combustible+
                ",\n    Año: "+this.año+",Recorrido: "+this.recorrido+", Precio: "+this.precio;
    }
}

