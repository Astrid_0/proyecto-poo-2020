/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

/**
 *
 * @author Hogar
 */
public class Motocicleta extends Vehiculo{
    private String tipo_de_moto;
    private String tipo_de_refrigeración;
    
    public Motocicleta(String placa, String marca, String modelo, String tipo_de_motor, String año, String color,String tipo_de_combustible, 
                       String transmisión, double recorrido, double precio, String tipo_de_moto, String tipo_de_refrigeración){
        super(placa, marca, modelo, tipo_de_motor, año, color, tipo_de_combustible, transmisión, recorrido, precio);
        this.tipo_de_moto= tipo_de_moto;
        this.tipo_de_refrigeración= tipo_de_refrigeración;
    }

    public String getTipo_de_moto() {
        return tipo_de_moto;
    }

    public void setTipo_de_moto(String tipo_de_moto) {
        this.tipo_de_moto = tipo_de_moto;
    }

    public String getTipo_de_refrigeración() {
        return tipo_de_refrigeración;
    }

    public void setTipo_de_refrigeración(String tipo_de_refrigeración) {
        this.tipo_de_refrigeración = tipo_de_refrigeración;
    }
    
    @Override
    public String toString(){
        return "Vehículo-Motocicleta: Placa: "+this.placa+", Marca: "+this.marca+", Modelo: "+this.modelo+", Tipo de motor: "+this.tipo_de_motor+", Transmisión: "+this.transmisión+
                ",\nTipo de combustible: "+this.tipo_de_combustible+", Color: "+this.color+", Tipo de moto: "+this.tipo_de_moto+", Tipo de refrigeración: "+this.tipo_de_refrigeración+
                ",\n    Recorrido: "+this.recorrido+", Año: "+this.año+", Precio: "+this.precio;
    }
}
