
package espol.edu.ec.common;

import java.io.Serializable;
import java.util.Objects;

public abstract class Persona implements Serializable{
    protected String nombre;
    protected String apellido;
    protected Cuenta cuenta;
    
    public Persona(String nombre, String apellido, Cuenta cuenta){
        this.nombre= nombre;
        this.apellido= apellido;
        this.cuenta= cuenta;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public Cuenta getCuenta() {
        return cuenta;
    }
    
    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
    
    @Override
    public String toString(){
        return "Persona: Nombre: "+this.nombre+", Apellido: "+this.apellido + ", Cuenta: "+ this.cuenta;
    }
     @Override
    public boolean equals(Object o){
    if(o==null || o.getClass()!=this.getClass())
        return false;
    if(o==this)
        return true;
    Persona p= (Persona)o;
    return Objects.equals(o,this);
    
    }
    
   
}
