
package espol.edu.ec.common;
import espol.edu.ec.util.Util;
        
public class Oferta {
    private double valor_ofertado;
    private Comprador comprador;
    private Venta venta;
    
    public Oferta(double valor_ofertado, Comprador comprador){
        this.valor_ofertado= valor_ofertado;
        this.comprador= comprador;
        this.venta= venta;
    }

    public double getValor_ofertado() {
        return valor_ofertado;
    }

    public Comprador getComprador() {
        return comprador;
    }

    public Venta getVenta() {
        return venta;
    }
    
    @Override
    public String toString(){
        return "Oferta: Placa: "+this.venta.getVehiculo().getPlaca()+", Comprador: "+this.comprador+" ,Valor ofertado: "+this.valor_ofertado;
    }
}
